//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by PredictEd.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_PREDICTED_DIALOG            102
#define IDR_MAINFRAME                   128
#define IDR_MENU_TOP                    132
#define IDD_DIALOG_PREWORDS             134
#define IDD_DIALOG_SETTINGS             136
#define IDB_BITMAP_NEW                  140
#define IDB_BITMAP_OPEN                 141
#define IDB_BITMAP_INS                  142
#define IDB_BITMAP_TRAIN                143
#define IDB_BITMAP_PASTE                146
#define IDB_BITMAP_COPY                 147
#define IDB_BITMAP_SAVEAS               148
#define IDB_BITMAP_SAVE                 149
#define IDB_BITMAP_CLEAR                151
#define IDB_BITMAP_FONTS                153
#define IDB_BITMAP_ABOUT                156
#define IDD_DIALOG_TRAIN                157
#define IDC_RICHEDIT21                  1000
#define IDC_BUTTON1                     1001
#define IDC_EDIT_WORDS                  1002
#define IDC_MFCLINK1                    1003
#define IDC_EDIT_WORDS2                 1003
#define IDC_EDIT_PRE                    1003
#define IDC_MFCLINK2                    1004
#define IDC_EDIT_W1                     1004
#define IDC_EDIT_W2                     1005
#define IDC_BUTTON_NEW                  1005
#define IDC_EDIT_W3                     1006
#define IDC_BUTTON_OPEN                 1006
#define IDC_EDIT_SET_LTMSZ              1006
#define IDC_EDIT_W4                     1007
#define IDC_BUTTON_SAVE                 1007
#define IDC_EDIT_SET_STMSZ              1007
#define IDC_EDIT_W5                     1008
#define IDC_BUTTON_SAVEAS               1008
#define IDC_BUTTON_SET_DEFFONT          1008
#define IDC_BUTTON_COPY                 1009
#define IDC_EDIT_P1                     1009
#define IDC_BUTTON_PASTE                1010
#define IDC_EDIT_SET_LIMIT              1010
#define IDC_EDIT_P2                     1010
#define IDC_BUTTON_CLEAR                1011
#define IDC_EDIT_SET_MARGINS            1011
#define IDC_EDIT_P3                     1011
#define IDC_BUTTON_FONTS                1012
#define IDC_BUTTON_SET_DEFFONT2         1012
#define IDC_BUTTON_SET_RESET            1012
#define IDC_BUTTON_TRAIN2               1013
#define IDC_BUTTON_TRAIN                1013
#define IDC_BUTTON_INS                  1014
#define IDC_MFCCOLORBUTTON_BK           1015
#define IDC_BUTTON_ADD                  1019
#define IDC_PROGRESS_TRAIN              1020
#define IDC_EDIT_PROG                   1021
#define IDC_BUTTON_START                1022
#define IDC_EDIT_RES                    1023
#define IDC_EDIT_ERR                    1024
#define ID_FILE_OPEN32771               32771
#define ID_FILE_SAVE32772               32772
#define ID_FILE_EXIT                    32773
#define ID_HELP_ONLINEHELP              32774
#define ID_HELP_CHECKFORUPDATES         32775
#define ID_HELP_ABOUTPREDICTED          32776
#define ID_OPTIONS_SETTINGS             32777
#define ID_OPTIONS_TRAIN                32778
#define ID_FILE_COPY                    32779
#define ID_OPTIONS_ERASEMEMORIES        32780
#define ID_FILE_SAVEPREDICTIONS         32788
#define ID_OPTIONS_MERGESTMINLTM        32789
#define ID_HELP_GETMOREFREEAPPS         32790
#define ID_FILE_SAVEFILEAS              32791
#define ID_FILE_PASTE                   32792
#define ID_FILE_                        32793
#define ID_OPTIONS_CLEARFORMATTING      32794
#define ID_OPTIONS_FONTANDSIZE          32795
#define ID_EDIT_FINDANDREPLACE          32796
#define ID_FILE_NEWFILE                 32797
#define ID_OPTIONS_LOADCONTEXT          32798
#define ID_OPTIONS_SAVECONTEXT          32799

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        159
#define _APS_NEXT_COMMAND_VALUE         32800
#define _APS_NEXT_CONTROL_VALUE         1023
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
